"""
API documentation:
http://www.sasbdb.org/rest-api/docs/

Current api_base.
"""
TEST_API_BASE = 'https://test-sasbdb.embl-hamburg.de/rest-api/'
API_BASE = 'https://www.sasbdb.org/rest-api/'
