#Requests library is required - http://docs.python-requests.org/
import requests
from base import API_BASE
import xml.etree.ElementTree as ET

SASBDB_codes = 'SASDAD7,SASDAA9,SASDAX8'
data = {'codes': SASBDB_codes}

try:
    resp = requests.post(API_BASE + 'entry/summary/list/', data, headers={"Accept":"application/xml"})
    if resp.status_code != 200:
        resp.raise_for_status()
    else:
        tree = ET.fromstring(resp.content)
        for entry in tree:
            print entry.find('code').text
            print entry.find('experiment').find('sample').find('name').text
            print entry.find('status').text
            
except requests.exceptions.HTTPError:
    resp.raise_for_status()
